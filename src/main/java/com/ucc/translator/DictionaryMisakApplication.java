package com.ucc.translator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DictionaryMisakApplication {

	public static void main(String[] args) {
		SpringApplication.run(DictionaryMisakApplication.class, args);
	}

}
