package com.ucc.translator.controller;


import com.ucc.translator.domain.dto.MasiveWordsUpdateForm;
import com.ucc.translator.domain.dto.SpanishDTO;
import com.ucc.translator.domain.dto.SpanishForm;
import com.ucc.translator.domain.entity.Words_spanish;
import com.ucc.translator.service.SpanishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SpanishController {

    @Autowired
    private SpanishService spanishService;

    @GetMapping("words_spanish")
    public ResponseEntity<List<Words_spanish>> getAllWords_SpanishOrderByWords() {
        return ResponseEntity.ok(spanishService.getAllWords_SpanishOrderByWords());
    }

    @GetMapping("word_spanis/{id}")
    public ResponseEntity<Words_spanish> getById(@PathVariable Long id) {
        Words_spanish words_spanish = this.spanishService.getById(id);

        if (words_spanish.getId() != null)
            return ResponseEntity.ok(words_spanish);
        else
            return ResponseEntity.notFound().build();
    }


    @GetMapping("words_spanish/{words}")
    public ResponseEntity<List<Words_spanish>> getAllByWordsIsLike(@PathVariable String words) {
        return ResponseEntity.ok(spanishService.getAllByWords_SpanishIsLike(words));
    }

    @GetMapping("words_spanish-search/{words}")
    public ResponseEntity<Words_spanish> getByWords(@PathVariable String words) {
        Words_spanish words_spanish = this.spanishService.getByWords(words);

        if (words_spanish.getId() != null)
            return ResponseEntity.ok(words_spanish);
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("words_misak/{words}/misakWords")
    public ResponseEntity<List<Words_spanish>> getAllSpanishByMisakWords(@PathVariable String words) {
        return ResponseEntity.ok(spanishService.getAllSpanishByMisakWords(words));
    }


    @PostMapping("words_spanish")
    public ResponseEntity<SpanishDTO> saveWords(@RequestBody SpanishForm form) {
        try {
            SpanishDTO words_spanish = this.spanishService.save(form);

            if (words_spanish.getId() != null)
                return ResponseEntity.ok(words_spanish);
            else
                return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("words_spanis/{id}")
    public ResponseEntity<String> deleteById(@PathVariable Long id) {
        try {
            boolean isDelete = this.spanishService.delecteById(id);
            if (isDelete)
                return ResponseEntity.ok("fue borrado con exito la palabra" + id);
            else
                return ResponseEntity.notFound().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }
    @PutMapping("words_spanish/{id}")
    public ResponseEntity<SpanishDTO> update(@RequestBody SpanishForm form, @PathVariable Long id) {
        try {
            SpanishDTO words_spanish = this.spanishService.update(form ,id);

            if (words_spanish.getId() != null)
                return ResponseEntity.ok(words_spanish);
            else
                return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("words_spanish/masive")
    public ResponseEntity<List<SpanishDTO>> updateMasive(@RequestBody MasiveWordsUpdateForm form) {
        try {
            List<SpanishDTO>spanishDTOList = this.spanishService.updateMasive(form );

            if (!spanishDTOList.isEmpty())
                return ResponseEntity.ok(spanishDTOList);
            else
                return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
    }
}
