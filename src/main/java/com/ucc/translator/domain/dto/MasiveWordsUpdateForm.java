package com.ucc.translator.domain.dto;

import java.util.List;

public class MasiveWordsUpdateForm {

private List<SpanishForm> formList;

    public List<SpanishForm> getFormList() {
        return formList;
    }

    public void setFormList(List<SpanishForm> formList) {
        this.formList = formList;
    }
}
