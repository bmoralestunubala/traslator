package com.ucc.translator.domain.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

@Entity
@Table(name = "spanish_misak", schema = "translator",
        uniqueConstraints = {
                @UniqueConstraint(
                        name = "spanish_misak_un", columnNames = {"id_spanish", "id_misak"})
        })
public class Misak_spanish {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "id_spanish", nullable = false)
    private Long idSpanish;

    @JoinColumn(name = "id_spanish", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JsonIgnore
    private  Words_spanish wordsSpanish;

    @Column(name = "id_misak", nullable = false)
    private Long idMisak;

    @JoinColumn(name = "id_misak", referencedColumnName = "id", updatable = false, insertable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JsonIgnore
    private  Words_misak words_misak;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdSpanish() {
        return idSpanish;
    }

    public void setIdSpanish(Long idSpanish) {
        this.idSpanish = idSpanish;
    }

    public Words_spanish getWordsSpanish() {
        return wordsSpanish;
    }

    public void setWordsSpanish(Words_spanish wordsSpanish) {
        this.wordsSpanish = wordsSpanish;
    }

    public Long getIdMisak() {
        return idMisak;
    }

    public void setIdMisak(Long idMisak) {
        this.idMisak = idMisak;
    }

    public Words_misak getWords_misak() {
        return words_misak;
    }

    public void setWords_misak(Words_misak words_misak) {
        this.words_misak = words_misak;
    }
}
