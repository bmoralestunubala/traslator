package com.ucc.translator.domain.entity;
import javax.persistence.*;

@Entity
@Table(name = "words_misak", schema = "translator")

public class Words_misak {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "words", nullable = false, length = 100)
    private String words;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }
}
