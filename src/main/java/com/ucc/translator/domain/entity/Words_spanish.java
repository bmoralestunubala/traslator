package com.ucc.translator.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "words_spanish", schema = "translator")


public class Words_spanish {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "words", nullable = false, length = 150)
    private String words;

    @Column(name = "description", nullable = false, length = 300)
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
