package com.ucc.translator.service;


import com.ucc.translator.domain.dto.MasiveWordsUpdateForm;
import com.ucc.translator.domain.dto.MisakDTO;
import com.ucc.translator.domain.dto.SpanishDTO;
import com.ucc.translator.domain.dto.SpanishForm;
import com.ucc.translator.domain.entity.Misak_spanish;
import com.ucc.translator.domain.entity.Words_misak;
import com.ucc.translator.domain.entity.Words_spanish;
import com.ucc.translator.domain.repository.MisakRepository;
import com.ucc.translator.domain.repository.SpanishMisakRepository;
import com.ucc.translator.domain.repository.SpanishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.List;
@Service
@Transactional
public class SpanishServiceImpl implements SpanishService {

    @Autowired
    private SpanishRepository spanishRepository;
    @Autowired
    private MisakRepository misakRepository;

    @Autowired SpanishMisakRepository spanishMisakRepository;
    @Override

    public List<Words_spanish> getAllWords_SpanishOrderByWords() {
        return spanishRepository.findAllByOrderByWords();
    }

    @Override
    public List<Words_spanish> getAllByWords_SpanishIsLike(String words) {
        return spanishRepository.getAllByWords_SpanishIsLike("%".concat(words).concat("%"));
    }

    @Override
    public Words_spanish getById(long id) {
        return this.spanishRepository.findById(id).orElse(new Words_spanish());
    }

    @Override
    public Words_spanish getByWords(String words) {
        return this.spanishRepository.findByWords(words).orElse(new Words_spanish());
    }

    @Override
    public List<Words_spanish> getAllSpanishByMisakWords(String misakWords) {
        return this.spanishRepository.getAllByMisakWords(misakWords);
    }


    @Override
    public SpanishDTO save(SpanishForm form)throws IllegalAccessException {
        //1. GUARDAR PRODUCTO
        Words_spanish words_spanish = new Words_spanish();
        words_spanish.setWords(form.getWords());
        words_spanish.setDescription(form.getDescription());
        words_spanish = spanishRepository.save(words_spanish);

// 2. validar categoria exista
       Words_misak words_misak = misakRepository.findById(form.getIdMisak())
                .orElseThrow(()-> new IllegalArgumentException("no existe el id misak"));

        // 3. guardar en tabla de relacion id words_misak e id words_spanish
        Misak_spanish misak_spanish = new Misak_spanish();
        misak_spanish.setIdMisak(words_misak.getId());
        misak_spanish.setIdSpanish(words_spanish.getId());
        spanishMisakRepository.save(misak_spanish);

//4 DEVOLVER INFORMACION MISAK
        SpanishDTO  responseDTO= new SpanishDTO();
        responseDTO.setId(words_spanish.getId());
        responseDTO.setWords(words_spanish.getWords());
        responseDTO.setDescription(words_spanish.getDescription());

//4 DEVOLVER INFORMACION MISAK
        MisakDTO misakDTO = new MisakDTO();
        misakDTO.setId(words_misak.getId());
        misakDTO.setWords(words_misak.getWords());
        responseDTO.setMisak(misakDTO);
        return responseDTO;
    }

    @Override
    public boolean delecteById(Long id) {
       // que exista la palabra

        Words_spanish words_spanish = spanishRepository.findById(id)
                .orElseThrow(()-> new IllegalArgumentException("no existe el id misak"));
        List<Misak_spanish> misak_spanishList = spanishMisakRepository.findAllByIdSpanish(words_spanish.getId());
        spanishMisakRepository.deleteAll(misak_spanishList);
        spanishRepository.deleteById(words_spanish.getId());

       // List<Words_misak> wordsMisakList = misakRepository.findAllByIdMisak(wo.getId());
        //spanishMisakRepository.deleteAll(misak_spanishList);
       // spanishRepository.deleteById(words_spanish.getId());
        return true;
    }

    @Override
    public SpanishDTO update(SpanishForm form, Long id) {

        Words_spanish oldWords = spanishRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException( "no existe "));
        //1. ACTUALIZAR PRODUCTO
        oldWords.setWords(form.getWords());
        oldWords.setDescription(form.getDescription());
        oldWords = spanishRepository.save(oldWords);


        //4 DEVOLVER INFORMACION MISAK
        SpanishDTO  responseDTO= new SpanishDTO();
        responseDTO.setId(oldWords.getId());
        responseDTO.setWords(oldWords.getWords());
        responseDTO.setDescription(oldWords.getDescription());

        return responseDTO;
    }

    @Override
    public List<SpanishDTO> updateMasive(MasiveWordsUpdateForm form) {
        List<SpanishDTO> responseList =new ArrayList<>();
        form.getFormList().forEach(updateForm-> responseList.add(update(updateForm, updateForm.getId())));

        return responseList;
    }
}


